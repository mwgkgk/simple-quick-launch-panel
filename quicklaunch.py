#!/usr/bin/env python2

import pygtk
pygtk.require('2.0')
import gtk
import os

items = [
    {'name':'dwb',
     'icon_path':'/usr/share/pixmaps/dwb.png',
     'action':'dwb','shortcut':'d'},
    {'name':'Firefox',
     'icon_path':'/usr/lib/firefox/browser/icons/mozicon128.png',
     'action':'firefox','shortcut':'f'},
    {'name':'Opera', 
     'icon_path':'/usr/share/opera/styles/images/Opera_256x256.png',
     'action':'opera','shortcut':'o'},
    {'name':'Chromium', 
     'icon_path':'/usr/share/icons/hicolor/256x256/apps/chromium.png',
     'action':'chromium --incognito','shortcut':'c'},
]


class QuickPanel:

    def execute_and_quit(self, widget, cmd):
        os.system(cmd + '&')
        gtk.main_quit()

    def keypress_callback(self, widget, data):
        keyname = gtk.gdk.keyval_name(data.keyval)

        if keyname == 'q':
            gtk.main_quit()

        if keyname in self.action_list:
            self.execute_and_quit(widget, self.action_list[keyname])


    def __init__(self):

        self.window = gtk.Dialog()
        self.window.set_position(gtk.WIN_POS_CENTER)

        self.window.connect("destroy", lambda wid: gtk.main_quit())
        self.window.connect("delete_event", lambda a1,a2:gtk.main_quit())
        self.window.connect("key_press_event", self.keypress_callback)

        self.action_list = {}

        for item in items:
            # Create an icon box :
            icon_box = gtk.VBox(False, 0)

            # Image
            image = gtk.Image()
            pixbuf = gtk.gdk.pixbuf_new_from_file(item['icon_path'])
            
            scale = 75
            if 'icon_scale' in item:
                scale = item['icon_scale']            
            scaled_buf = pixbuf.scale_simple(scale,scale,gtk.gdk.INTERP_BILINEAR)
            image.set_from_pixbuf(scaled_buf)

            # Create a label for the button
            label_name = gtk.Label(item['name'])
            label_shortcut = gtk.Label(item['shortcut'])

            # Pack the pixmap and label into the box
            icon_box.pack_end(label_shortcut, False, False, 3)
            icon_box.pack_end(label_name, False, False, 3)
            icon_box.pack_end(image, False, False, 3)

            image.show()
            label_name.show()
            label_shortcut.show()

            # Create a button :
            button = gtk.Button()
            button.connect("clicked", self.execute_and_quit, item['action'])
            button.add(icon_box)

            # Bind the shortcut :
            self.action_list[item['shortcut']] = item['action']

            self.window.action_area.pack_start(button, True, True, 0)

            icon_box.show()
            button.show()

        self.window.show()

    def main(self):
        gtk.main()

if __name__ == '__main__':
    quickpanel = QuickPanel()
    quickpanel.main()
